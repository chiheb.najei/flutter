import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';

class ProductDetails extends StatefulWidget {
  final String _id;
  final String _image;
  final String _title;
  final String _description;
  final int _price;
  final int _quantity;

  const ProductDetails(this._id, this._image, this._title, this._description,
      this._price, this._quantity,
      {Key? key})
      : super(key: key);

  @override
  State<ProductDetails> createState() => _ProductDetailsState();
}

class _ProductDetailsState extends State<ProductDetails> {
  late int _currentQuantity;

  final String _baseUrl = "10.0.2.2:9090";

  @override
  void initState() {
    _currentQuantity = widget._quantity;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget._title),
      ),
      body: Column(
        children: [
          Container(
              width: double.infinity,
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 20),
              child: Image.network("http://10.0.2.2:9090/img/" + widget._image,
                  width: 460, height: 215)),
          Container(
            margin: const EdgeInsets.fromLTRB(20, 0, 20, 50),
            child: Text(widget._description),
          ),
          Text(widget._price.toString() + " TND", textScaleFactor: 3),
          Text("Exemplaires disponibles : " + _currentQuantity.toString()),
          const SizedBox(
            height: 50,
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Map<String, String> headers = {
            "Content-Type": "application/json; charset=UTF-8"
          };
          http
              .get(
                  Uri.http(_baseUrl,
                      "library/617736fbfae2b4afb1912410/" + widget._id),
                  headers: headers)
              .then((http.Response response) {
            if (response.statusCode == 200) {
              Map<String, dynamic> responseMap = json.decode(response.body);
              if (widget._quantity > 0) {
                if (responseMap["count"] > 0) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return const AlertDialog(
                          title: Text("Information"),
                          content: Text("Vous possédez déjà ce jeu !"),
                        );
                      });
                } else {
                  Map<String, dynamic> postData = {
                    "user": "617736fbfae2b4afb1912410",
                    "game": widget._id,
                  };
                  Map<String, String> headers = {
                    "Content-Type": "application/json; charset=UTF-8"
                  };
                  http
                      .post(Uri.http(_baseUrl, "/library"),
                          headers: headers, body: json.encode(postData))
                      .then((http.Response response) {
                    if (response.statusCode == 200) {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return const AlertDialog(
                            title: Text("informations"),
                            content: Text("Ajouté à la libraire avec succés !"),
                          );
                        },
                      );
                      setState(() {
                        _currentQuantity--;
                      });
                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return const AlertDialog(
                              title: Text("Informationn"),
                              content: Text(
                                  "Une erreur s'est produite, réessayer plus tard !"),
                            );
                          });
                    }
                  });
                }
              } else {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return const AlertDialog(
                        title: Text("Information"),
                        content: Text("Stock épuisé !"),
                      );
                    });
              }
            }
          });
        },
        label: const Text("Acheter", textScaleFactor: 2),
        icon: const Icon(Icons.shopping_basket_rounded),
      ),
    );
  }
}
